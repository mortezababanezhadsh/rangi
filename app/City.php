<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'name', 'en_name', 'area_code', 'latitude', 'longitude', 'approved'
    ];

    public function province()
    {
        return $this->belongsTo('App\Province');
    }
}
