<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id', 'address_id', 'products', 'total_price'
    ];

    public function transactions()
    {
        return $this->HasMany('App\Transaction');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
