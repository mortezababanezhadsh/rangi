<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'name', 'family', 'phone', 'address', 'postal_code', 'description', 'state_id', 'city_id',
    ];
    
    public function user()
    {
        return $this->hasOne('App\User');
    }
}
