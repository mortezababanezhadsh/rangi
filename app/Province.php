<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'province_id', 'name', 'en_name', 'latitude', 'longitude', 'approved'
    ];

    public function cities()
    {
        return $this->hasMany('App\City');
    }
}
