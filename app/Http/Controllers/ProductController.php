<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->simplePaginate(10);

        return view('ui.product.index', compact('products'));
    }

    public function single(Product $product)
    {
        $comments = $product->comments()->where('status', 1)->where('reply_id', 0)->latest()->with(['childs' => function ($query) {
            $query->where('status', 1)->latest();
        }])->simplePaginate(5);
        
        return view('ui.product.single', compact('product', 'comments'));
    }

    public function addCart($request)
    {
        dd($request->all());
    }
}
