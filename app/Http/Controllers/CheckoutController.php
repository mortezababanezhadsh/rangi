<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Province;
use App\City;
use App\Coupon;
use App\Transportation;


class CheckoutController extends Controller
{
    public function index()
    {
        if (! auth()->user()) {
            return redirect(route('cart.index'))->with(['message' => 'برای ادامه ثبت سفارش ابتدا وارد حساب کاربری خود شوید']);
        } 

        $this->middleware('auth');

        $products = json_decode(request()->cookie('products'), true);

        if (! $products) {
            return redirect(route('cart.index'))->with(['message' => 'برای ثبت سفارش محصولی در سبد خرید وجود ندارد!']);
        }

        $totalPayment = 0;

        foreach ($products as $key => $product) {
            $totalPayment += $product['price'] * $product['quantity'];
        }


        $provinces = Province::all();
        $transportations = Transportation::whereStatus(true)->get();

        $discount_price = 0;
        $total = $totalPayment;

        $transportation_price = intval($transportations->first()->price);
        $totalPayment = $totalPayment + $transportation_price;

        return view('ui.pages.checkout', compact('products', 'provinces', 'transportations', 'total', 'discount_price', 'transportation_price', 'totalPayment'));
    }


    public function getCity(Request $request)
    {
        return City::where("province_id", $request->province_id)->get();
    }


    public function applyDiscount(Request $request)
    {
        if (! trim($request->code) == '' || $request->code == null) {
            $code = Coupon::whereCode(trim($request->code))->whereStatus(true)->first();
            
            if ($code !== null && \Carbon\Carbon::now() < \Carbon\Carbon::parse($code['expires_at'])) {

                // check for user is use the code or not
                if (! in_array(trim($code->id) , auth()->user()->coupons->pluck('id')->toArray())) {
                    auth()->user()->coupons()->attach($code->id);

                    $products = json_decode(request()->cookie('products'), true);

                    if (! $products) {
                        return redirect(route('cart.index'))->with(['message' => 'برای ثبت سفارش محصولی در سبد خرید وجود ندارد!']);
                    }

                    $totalPayment = 0;
                    $total = 0;

                    // start check for define transportation price
                    foreach ($products as $key => $product) {
                        $total += $product['price'] * $product['quantity'];
                    }

                    if ($request->transportation_id) {
                        $transportation = Transportation::findOrFail($request->id);
                        $transportation_price = intval($transportation->price);
                    }else{
                        $transportations = Transportation::whereStatus(true)->get();
                        $transportation_price = intval($transportations->first()->price);
                    }
                    // end check for define transportation price

                    // calculate discount 
                    $discount_price = 0;
                    if ($code) {
                        $discount_percentage = $code['discount_amount'] * 1/100;
                        $discount_price = $totalPayment * $discount_percentage;
                    }
                    //end calculate discount 

                    // calculate total payment 
                    $totalPayment = $totalPayment + $transportation_price;
                    $totalPayment = $totalPayment - $discount_price;

                    return response(['discount_price' => $discount_price, 'totalPayment' => $totalPayment, 'transportation_price' => $transportation_price]);
                }else{
                    return response(['message' => 'این کد توسط شما یک بار استفاده شده است!']);
                }
                // end check for user is use the code or not
            }else{
                return response(['message' => 'کد تخفیف شما معتبر نیست!']);
            }
        }else{
            return response(['message' => 'کد تخفیف خود را وارد کنید']);
        }
    }

    public function changePriceTransportation(Request $request)
    {
        if ($request->id) {
            $transportation = Transportation::findOrFail($request->id);
            $transportation_price = intval($transportation->price);
            return response(['transportation_price' => $transportation_price]);
        }
    }
}
