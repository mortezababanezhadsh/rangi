<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Coupon;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ui.pages.cart');
    }

    public function store(Request $request)
    {
        
        $id = $request->id;
        $product = Product::findOrFail($id);

        if (request()->cookie('products')) {
            $products = json_decode(request()->cookie('products'), true);
        } else {
            $products = [];
        }

        // if cart is empty then this the first product
        if (!$products) {

            $products = [
                $id => [
                    "title" => $product->title,
                    "body" => $product->body,
                    "slug" => $product->slug,
                    "quantity" => 1,
                    "price" => $product->price,
                    "image" => $product->images[0]->url
                ]
            ];

            return response(['message' => 'محصول مورد نظر به سبد خرید شما افزوده شد'])->cookie('products', json_encode($products), 60 * 24 * 365);
        }

        // if cart not empty then check if this product exist then increment quantity
        if (array_key_exists($product->id, $products)) {
            $products[$id]['quantity']++;

            return response(['message' => 'محصول مورد نظر به سبد خرید شما افزوده شد'])->cookie('products', json_encode($products));
        }

        // if item not exist in cart then add to cart with quantity = 1
        $products[$id] = [
            "title" => $product->title,
            "body" => $product->body,
            "slug" => $product->slug,
            "quantity" => 1,
            "price" => $product->price,
            "image" => $product->images[0]->url
        ];

        return response(['message' => 'محصول مورد نظر به سبد خرید شما افزوده شد'])->cookie('products', json_encode($products), 60 * 24 * 365);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $quantity = $request->quantity;

        $product = Product::findOrFail($id);

        if (request()->cookie('products')) {
            $products = json_decode(request()->cookie('products'), true);
        } 
        
        // if cart not empty then check if this product exist then increment quantity
        if (array_key_exists($product->id, $products)) {

            $products[$id]['quantity']++;

            return response(['message' => 'محصول مورد نظر به سبد خرید شما افزوده شد'])->cookie('products', json_encode($products));
        }
    }

    public function destroy(Request $request)
    {
        if ($request->id) {

            if (request()->cookie('products')) {
                $products = json_decode(request()->cookie('products'), true);
            }
            if (array_key_exists($request->id, $products)) {
                unset($products[$request->id]);
            }
            return response(['message' => 'محصول مورد نظر از سبد خرید شما حذف شد'])->cookie('products', json_encode($products)); 
        }
    }
}
