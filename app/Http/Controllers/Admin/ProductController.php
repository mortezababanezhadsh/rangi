<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Image;
use App\Product;
use App\Tag;
use Illuminate\Http\Request;

class ProductController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereStatus(1)->get();
        $tags = Tag::whereStatus(1)->get();
        return view('admin.products.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(ProductRequest $request)
    {
        if ($request->file('images')) {
            $urls = $this->uploadImages($request->file('images'));
        }
        $product = auth()->user()->products()->create($request->all());
        
        foreach ($urls as $url) {
            Image::create([
                'url' => $url,
                'imageable_id' => $product->id,
                'imageable_type' => get_class($product),
            ]);
        }
        $product->categories()->sync($request->input('category_id'));

        if ($request->input('tag_id')) {
            foreach ($request->input('tag_id') as $id) {
                $product->tags()->attach(array_merge(['tag_id' => $id]), [
                    'taggable_id' => $product->id,
                    'taggable_type' => get_class($product),
                ]);
            }
        }

        return redirect(route('products.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::whereStatus(1)->get();
        $tags = Tag::whereStatus(1)->get();
        return view('admin.products.edit', compact('product', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        $file = $request->file('images');
        if ($file) {
            $urls = $this->uploadImages($request->file('images'));
            foreach ($urls as $url) {
                Image::create([
                    'url' => $url,
                    'imageable_id' => $product->id,
                    'imageable_type' => get_class($product),
                ]);
            }
        }

        $product->update($request->all());
        $product->categories()->sync($request->input('category_id'));

        if ($request->input('tag_id')) {
            foreach ($request->input('tag_id') as $id) {
                $product->tags()->sync(array_merge(['tag_id' => $id]), [
                    'taggable_id' => $product->id,
                    'taggable_type' => get_class($product),
                ]);
            }
        }

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->delete();
    }

    public function deleteImage(Request $request)
    {
        $image = Image::findOrFail($request->id);
        $image->delete();
    }

    public function status(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->status = $request->status;
        $product->save();
    }
}
