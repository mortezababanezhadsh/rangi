<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Tag;
use App\Image;
use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;

class ArticleController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereStatus(1)->get();
        $tags = Tag::whereStatus(1)->get();
        return view('admin.articles.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $url = $this->uploadImages($request->file('image'));

        $article = auth()->user()->articles()->create($request->all());
        Image::create([
            'url' => $url,
            'imageable_id' => $article->id,
            'imageable_type' => get_class($article),
        ]);

        $article->categories()->sync($request->input('category_id'));

        if ($request->input('tag_id')) {
            foreach ($request->input('tag_id') as $id) {
                $article->tags()->attach(array_merge(['tag_id' => $id]), [
                    'taggable_id' => $article->id,
                    'taggable_type' => get_class($article),
                ]);
            }
        }

        return redirect(route('articles.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories = Category::whereStatus(1)->get();
        $tags = Tag::whereStatus(1)->get();
        return view('admin.articles.edit', compact('article', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $file = $request->file('image');
        if ($file) {
            $url = $this->uploadImages($request->file('image'));
            $article->image->update([
                'url' => $url
            ]);
        }

        $article->update($request->all());
        $article->categories()->sync($request->input('category_id'));

        if ($request->input('tag_id')) {
            foreach ($request->input('tag_id') as $id) {
                $article->tags()->sync(array_merge(['tag_id' => $id]), [
                    'taggable_id' => $article->id,
                    'taggable_type' => get_class($article),
                ]);
            }
        }

        return redirect(route('articles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $article = Article::findOrFail($request->id);
        $article->delete();
    }

    public function status(Request $request)
    {
        $article = Article::findOrFail($request->id);
        $article->status = $request->status;
        $article->save();
    }
}
