<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CouponRequest;

class CouponController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin.coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(\Morilog\Jalali\CalendarUtils::toGregorian("1398", "11", "28"));
        return view('admin.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(CouponRequest $request)
    {
        $expires_at = $this->shamsiToMiladi($request->expires_at);

        $coupon = Coupon::create(array_merge($request->all(), ['expires_at' => $expires_at]));
     
        return redirect(route('coupons.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        return view('admin.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(CouponRequest $request, Coupon $coupon)
    {
        $expires_at = $this->shamsiToMiladi($request->expires_at);

        $coupon->update(array_merge($request->all(), ['expires_at' => $expires_at]));

        // if ($request->extra_at) {
        //     $expires_at->addDays($request->extra_days);
        // }

        return redirect(route('coupons.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
    }

    public function status(Request $request)
    {
        $coupon = Coupon::findOrFail($request->id);
        $coupon->status = $request->status;
        $coupon->save();
    }
}
