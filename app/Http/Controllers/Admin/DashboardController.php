<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DashboardController extends AdminController
{
    public function index()
    {
        return view('admin.dashboard');
    }


    public function uploadImageSubject()
    {
        $this->validate(request(), [
            'upload' => 'required|mimes:jpeg,png,bmp',
        ]);
        $file = request()->file('upload');

        $url = $this->uploadImages($file);
        $url = asset('storage/' . $url);

        return "<script>window.parent.CKEDITOR.tools.callFunction(1 , '{$url}' , '')</script>";
    }
}
