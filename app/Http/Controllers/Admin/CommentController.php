<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function index()
    {
        $comments = Comment::where('status', 1)->latest()->get();
        return view('admin.comments.index', compact('comments'));
    }

    public function unsuccessful()
    {
        $comments = Comment::where('status', 0)->latest()->get();
        return view('admin.comments.approved', compact('comments'));
    }

    public function delete(Request $request)
    {
        $comment = Comment::findOrFail($request->id);
        $comment->delete();
    }

    public function status(Request $request)
    {
        $comment = Comment::findOrFail($request->id);
        $comment->status = $request->status;
        $comment->save();
    }
}
