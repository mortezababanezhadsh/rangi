<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;


class AdminController extends Controller
{
    public function uploadImages($files)
    {
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $imagePath = "/uploads/images/$year/$month/$day";
        if ($files) {
            if (is_array($files)) {
                foreach ($files as $file) {
                    $url[] = $file->store($imagePath, 'public');
                }
            } else {
                $url = $files->store($imagePath, 'public');
            }
        }

        return $url;
    }

    public function shamsiToMiladi($date)
    {
        // $expires_at = $request->expires_at;
        // https://github.com/morilog/jalali   =>  description is end page
        $dateString = \Morilog\Jalali\CalendarUtils::convertNumbers($date, true); // 1395-02-19

        return \Morilog\Jalali\CalendarUtils::createCarbonFromFormat('Y-m-d', $dateString)->format('Y-m-d H:i:s'); //2016-05-8
    }
}
