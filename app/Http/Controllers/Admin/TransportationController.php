<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Transportation;
use Illuminate\Http\Request;
use App\Http\Requests\TransportationRequest;

class TransportationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transportations = Transportation::all();
        return view('admin.transportations.index', compact('transportations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.transportations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransportationRequest $request)
    {
        Transportation::create($request->all());
        return redirect(route('transportations.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transportation  $transportation
     * @return \Illuminate\Http\Response
     */
    public function edit(Transportation $transportation)
    {
        return view('admin.transportations.edit', compact('transportation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transportation  $transportation
     * @return \Illuminate\Http\Response
     */
    public function update(TransportationRequest $request, Transportation $transportation)
    {
        $transportation->update($request->all());
        return redirect(route('transportations.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transportation  $transportation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transportation $transportation)
    {
        $transportation->delete();
    }

    public function status(Request $request)
    {
        $transportation = Transportation::findOrFail($request->id);
        $transportation->status = $request->status;
        $transportation->save();
    }
}
