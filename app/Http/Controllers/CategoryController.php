<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index(Category $category)
    {
        $articles = $category->articles()->simplePaginate(10);
        return view('ui.blog.index', compact('articles'));
    }
}
