<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('ui.pages.home');
    }

    public function comment()
    {
        $this->validate(request(),[
            'comment' => ['required']
        ]);

        auth()->user()->comments()->create(request()->input('comment'));
        return back();
    }
}
