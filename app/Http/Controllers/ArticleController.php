<?php

namespace App\Http\Controllers;

use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::latest()->simplePaginate(10);
        $sidebarPost = Article::latest()->take(5)->get();

        return view('ui.blog.index', compact('articles', 'sidebarPost'));
    }

    public function single(Article $article)
    {
        $comments = $article->comments()->where('status', 1)->where('reply_id', 0)->latest()->with(['childs' => function ($query) {
            $query->where('status', 1)->latest();
        }])->simplePaginate(5);
        
        $sidebarPost = Article::latest()->take(5)->get();
        return view('ui.blog.single', compact('article', 'comments', 'sidebarPost'));
    }
}
