<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'POST') {
            return [
                'title' => ['required', 'max:255', 'min:5'],
                'description' => ['required', 'min:5'],
                'body' => ['required', 'min:5'],
                'image' => ['required', 'mimes:jpeg,png,bmp'],

            ];
        }

        return [
            'title' => ['required', 'max:255', 'min:5'],
            'description' => ['required', 'min:5'],
            'body' => ['required', 'min:5'],
        ];
    }
}
