<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('ui.layouts.sidebar', function ($view) {
            $categories = \App\Category::all();
            $tags = \App\Tag::all();
            $view->with(compact('categories', 'tags'));
        });

        view()->composer('ui.layouts.header', function ($view) {
            $menus = \App\Menu::where('parent_id', 0)->get();
            $view->with(compact('menus'));
        });

        view()->composer('ui.layouts.footer', function ($view) {
            $menus = \App\Menu::where('parent_id', 0)->get();
            $view->with(compact('menus'));
        });
    }
}
