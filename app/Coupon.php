<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
  protected $fillable = [
    'code', 'description', 'discount_amount', 'uses', 'extra_at', 'expires_at'
  ];

  public function users()
  {
    return $this->belongsToMany('App\User');
  }

  public function products()
  {
    return $this->belongsToMany('App\Product');
  }
}