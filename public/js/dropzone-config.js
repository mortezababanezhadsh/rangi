var total_photos_counter = 0;
var name = "";
Dropzone.options.myDropzone = {
    uploadMultiple: true,
    parallelUploads: 2,
    autoProcessQueue: false,
    maxFilesize: 5,
    previewTemplate: document.querySelector("#preview").innerHTML,
    addRemoveLinks: true,
    dictRemoveFile: "حذف تصویر",
    dictFileTooBig: "تصویر بزرگتر از  5 MB است.",
    timeout: 10000,

    init: function() {
        this.on("removedfile", function(file) {
            $.ajax({
                method: "POST",
                url: "/admin/products/dropzone/delete",
                data: {
                    id: file.customName,
                    _token: $('[name="_token"]').val()
                },
                dataType: "json",
                success: function(data) {
                    total_photos_counter--;
                    $("#counter").text("# " + total_photos_counter);
                }
            });
        });
    },
    success: function(file, done) {
        total_photos_counter++;
        $("#counter").text("# " + total_photos_counter);
        file["customName"] = name;
    }
};
