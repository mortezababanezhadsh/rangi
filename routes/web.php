<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// UI Routes
Route::get('/', 'HomeController@index')->name('home');

Route::get('/blog', 'ArticleController@index');
Route::get('/articles/{articleSlug}', 'ArticleController@single');

Route::get('/products', 'ProductController@index');
Route::get('/products/{productSlug}', 'ProductController@single');

Route::resource('/cart', 'CartController')->except(['show']);
// Route::post('/cart/quantity-change', 'CartController@quantityChange')->name('cart.quantityChange');


Route::prefix('checkout')->group(function () {
    Route::get('/', 'CheckoutController@index')->name('checkout.index');
    Route::post('/city', 'CheckoutController@getCity')->name('checkout.city');
    Route::post('/apply-discount', 'CheckoutController@applyDiscount')->name('checkout.applyDiscount');
    Route::post('/change-price-transportation', 'CheckoutController@changePriceTransportation')->name('checkout.changePriceTransportation');
});

Route::post('/comment', 'HomeController@comment')->middleware('auth');
Route::get('/articles/category/{categorySlug}', 'CategoryController@index')->name('categories');
Route::get('/articles/tag/{tagSlug}', 'TagController@index')->name('tags');

// ADMIN Routes
Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::post('/dashboard/upload-image', 'DashboardController@uploadImageSubject')->name('admin.upload');

    Route::resource('articles', 'ArticleController')->except(['show', 'destroy']);
    Route::get('/articles/status/update', 'ArticleController@status')->name('articles.status');
    Route::post('/articles/delete', 'ArticleController@delete')->name('articles.delete');

    Route::resource('products', 'ProductController')->except(['show', 'destroy']);
    Route::get('/products/status/update', 'ProductController@status')->name('products.status');
    Route::post('/products/delete', 'ProductController@delete')->name('products.delete');
    Route::post('/products/delete-image', 'ProductController@deleteImage')->name('products.deleteImage');

    Route::resource('categories', 'CategoryController')->except(['show', 'destroy']);
    Route::get('/categories/status/update', 'CategoryController@status')->name('categories.status');
    Route::post('/categories/delete', 'CategoryController@delete')->name('categories.delete');

    Route::resource('menus', 'MenuController')->except(['show', 'destroy']);
    Route::get('/menus/status/update', 'MenuController@status')->name('menus.status');
    Route::post('/menus/delete', 'MenuController@delete')->name('menus.delete');

    Route::resource('tags', 'TagController')->except(['show', 'destroy']);
    Route::get('/tags/status/update', 'TagController@status')->name('tags.status');
    Route::post('/tags/delete', 'TagController@delete')->name('tags.delete');

    Route::get('/comments', 'CommentController@index')->name('comments.index');
    Route::get('/comments/status/update', 'CommentController@status')->name('comments.status');
    Route::post('/comments/delete', 'CommentController@delete')->name('comments.delete');
    Route::get('/comments/unsuccessful', 'CommentController@unsuccessful')->name('comments.approved');

    Route::resource('roles', 'RoleController')->except(['show', 'destroy']);
    Route::post('/roles/delete', 'RoleController@delete')->name('roles.delete');

    Route::resource('permissions', 'PermissionController')->except(['show', 'destroy']);
    Route::post('/permissions/delete', 'PermissionController@delete')->name('permissions.delete');

    Route::resource('coupons', 'CouponController')->except(['show']);
    Route::get('/coupons/status/update', 'CouponController@status')->name('coupons.status');

    Route::resource('transportations', 'TransportationController')->except(['show']);
    Route::get('/transportations/status/update', 'TransportationController@status')->name('transportations.status');
});
