@extends('ui.layouts.master')

@section('content')
<section class="login_part section_padding ">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6">
                <div class="login_part_text text-center">
                    <div class="login_part_text_iner">
                        <h2>برای اولین بار از فروشگاه ما دیدن میکنید?</h2>
                        <p>برای خرید از فروشگاه نیاز است ابتدا در سایت ثبت نام کنید.</p>
                        <a href="{{ route('register') }}" class="btn_3 btn-sm">ایجاد حساب کاربری</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="login_part_form">
                    <div class="login_part_form_iner">
                        <h3>خوش برگشتی !</h3>
                        <form class="row contact_form" action="{{ route('login') }}" method="post" novalidate="novalidate">
                            @csrf
                            <div class="col-md-12 form-group p_star">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" name="email" value="{{ old('email') }}" placeholder="ایمیل" autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-12 form-group p_star">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" value="" placeholder="رمز عبور" autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="creat_account d-flex align-items-center">
                                    <input type="checkbox" id="f-option" name="selector" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="f-option">به خاطر سپردن؟</label>
                                </div>
                                <button type="submit" value="submit" class="btn_3 btn-sm">
                                ورود
                                </button>
                                @if (Route::has('password.request'))
                                <a class="lost_pass" href="{{ route('password.request') }}">فراموشی رمز عبور?</a>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</section>
@endsection
