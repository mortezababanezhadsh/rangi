@extends('admin.layouts.master')
@section('title' , 'ویرایش مجوزها')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
          <div class="card-box">
              <div class="head-section">
                  <h4>ویرایش مجوز</h4>
                  <a href="{{ route('permissions.index') }}" class="btn btn-primary btn-sm">مشاهده مجوزها</a>
              </div>
              <hr>
              @include('admin.section.errors')
              <div class="row">
                <div class="col-lg-12">
                  <form class="form-horizontal" action="{{ route('permissions.update' , ['permission'=> $permission->id]) }}" method="post">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                      <label for="name">نام مجوز</label>
                      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ $permission->name }}">
                    </div>
                    <div class="form-group">
                      <label for="description">توضیحات مجوز</label>
                      <input type="text" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" value="{{ $permission->description }}" />
                    </div>
                    <br>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-block">ویرایش مجوز</button>
                      <a href="{{ route('permissions.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>     
    </div>
  </section>
@endsection