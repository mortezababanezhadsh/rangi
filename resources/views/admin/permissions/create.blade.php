@extends('admin.layouts.master')
@section('title' , 'ثبت مجوز')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
        <div class="card-box">
            <div class="head-section">
                <h4>ثبت مجوز جدید</h4>
                <a href="{{ route('permissions.index') }}" class="btn btn-primary btn-sm">مشاهده مجوز ها</a>
            </div>
            {{-- <p class="text-danger">فقط برنامه نویسان اپلیکیشن مجاز به ثبت مجوز و فعال سازی آن هستند!</p> --}}
            <hr>
            @include('admin.section.errors')
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" action="{{ route('permissions.store') }}" method="post">
                @csrf
                  <div class="form-group">
                    <label for="name">نام مجوز</label> <small><span class="text-danger">به صورت انگلیسی نوشته شود.</span></small>
                    <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{old('name')}}">
                  </div>
                  <div class="form-group">
                    <label for="description">توضیحات مجوز</label>
                    <input type="text" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" value="{{old('description')}}" />
                  </div>
                  <br>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">ثبت مجوز</button>
                    <a href="{{ route('permissions.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>     
    </div>
  </section>
@endsection