@if ($errors->any())
    <div class="alert alert-danger">
        <strong>خطا!</strong> لطفا موارد زیر را بررسی نمائید.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif