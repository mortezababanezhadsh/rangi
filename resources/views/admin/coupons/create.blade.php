@extends('admin.layouts.master')
@section('title' , 'ثبت کوپن')

@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/persian-datepicker@1.2.0/dist/css/persian-datepicker.min.css">
@endsection

@section('scripts')
    <script src="https://unpkg.com/persian-date@1.1.0/dist/persian-date.min.js"></script>
    <script src="https://unpkg.com/persian-datepicker@1.2.0/dist/js/persian-datepicker.min.js"></script>
    <script>
      $('#expires_at').persianDatepicker({
        initialValue: false,
        format: 'YYYY-MM-DD',
      });
    </script>
@endsection

@section('content')
<section>
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="head-section">
          <h4>ثبت کوپن جدید</h4>
          <a href="{{ route('coupons.index') }}" class="btn btn-primary btn-sm">مشاهده کوپن ها</a>
        </div>
        <hr>
        @include('Admin.section.errors')
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="{{ route('coupons.store') }}" method="post">
              @csrf
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="code">کد تخفیف</label>
                    <input type="text" class="form-control" name="code" value="{{old('code')}}">
                </div>
                  <div class="col-md-6">
                      <label for="description">توضیحات</label>
                      <input type="text" class="form-control" name="description" value="{{old('description')}}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="expires_at">تاریخ انقضا تخفیف</label>
                  <input class="form-control" name="expires_at" id="expires_at" value="{{old('expires_at')}}">
                  </div>
                  <div class="col-md-6">
                    <label for="discount_amount"> <span class="text-danger">درصد</span> میزان تخفیف</label>
                    <input type="number" min="1" max="100" class="form-control" name="discount_amount" value="{{old('discount_amount')}}">
                  </div>
                </div>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">ثبت کوپن</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
