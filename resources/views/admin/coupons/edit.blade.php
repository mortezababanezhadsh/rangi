@extends('admin.layouts.master')
@section('title' , 'ویرایش کوپن')

@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/persian-datepicker@1.2.0/dist/css/persian-datepicker.min.css">
@endsection

@section('scripts')
    <script src="https://unpkg.com/persian-date@1.1.0/dist/persian-date.min.js"></script>
    <script src="https://unpkg.com/persian-datepicker@1.2.0/dist/js/persian-datepicker.min.js"></script>
    <script>
      $('#expires_at').persianDatepicker({
        initialValue: false,
        format: 'YYYY-MM-DD',
      });
    </script>
@endsection

@section('content')
<section>
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="head-section">
          <h4>ویرایش کوپن</h4>
          <a href="{{ route('coupons.index') }}" class="btn btn-primary">مشاهده کوپن ها</a>
        </div>
        <hr>
        @include('Admin.section.errors')
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="{{ route('coupons.update', ['coupon' => $coupon->id]) }}"
              method="post">
              @csrf
              @method('PATCH')
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="code">کد تخفیف</label>
                    <input type="text" class="form-control" name="code" value="{{$coupon->code}}">
                </div>
                  <div class="col-md-6">
                      <label for="description">توضیحات</label>
                      <input type="text" class="form-control" name="description" value="{{$coupon->description}}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="expires_at">تاریخ انقضا</label>
                  <input id="expires_at" class="form-control" name="expires_at" value="{{ jdate($coupon->expires_at)->format('%d %B، %Y') }}">
                  </div>
                  <div class="col-md-6">
                    <label for="discount_amount"> <span class="text-danger">درصد</span> میزان تخفیف</label>
                    <input id="" max="100" class="form-control" name="discount_amount" value="{{$coupon->discount_amount}}">
                  </div>
                </div>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">ویرایش کوپن</button>
                <a href="{{ route('coupons.index') }}" class="btn btn-secondary btn-block">کنسل</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection