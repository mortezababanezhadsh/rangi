@extends('admin.layouts.master')
@section('title' , 'ویرایش محصول')

@section('styles')

{{--bootstrap FileInput--}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<style>
  .file-drop-zone{
    overflow: auto;
  }
</style>
@endsection
@section('scripts')

  <!-- the main fileinput plugin file -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/fileinput.min.js"></script>
  <script>
  $(function () {
    $("#images").fileinput();
  });
  </script>

  <script>
    
    $('.deleteImage').on('click', function (e) {
        e.preventDefault();

        let le = $(this);
        let url = le.attr('href');
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let id = $(this).data('id');

        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'مطمئن هستید?',
            text: "بعد از حذف شما قادر به بازیابی اطلاعات نیستید!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'حذف',
            cancelButtonText: 'کنسل',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        'id': id,
                        '_token': CSRF_TOKEN
                    },
                    success: function (res) {  
                        
                        swalWithBootstrapButtons.fire(
                                'حذف شد!',
                                'اطلاعات شما به طور کامل حذف شد',
                                'success'
                            )
                            setInterval(function() {location.reload();}, 2000);
                    }
                });
            }else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                'کنسل شد',
                'اطلاعات موردنظر هنوز در دسترس شماست :)',
                'error'
                )
            }
        });
    });
  </script>
<script src="//cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
<script>
  var editor = CKEDITOR.replace('body', {
    language: 'fa',
    filebrowserUploadMethod : 'form',
    filebrowserUploadUrl: "{{route('admin.upload')}}"
  });
</script>
@endsection

@section('content')
<section>
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="head-section">
          <h4>ویرایش محصول</h4>
          <a href="{{ route('products.index') }}" class="btn btn-primary">مشاهده مقالات</a>
        </div>
        <hr>
        @include('Admin.section.errors')
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="{{ route('products.update', ['product' => $product->id]) }}"
              method="post" enctype="multipart/form-data">
              @csrf
              @method('PATCH')
              <div class="form-group">
                <label for="title">عنوان محصول</label>
                <input type="text" class="form-control" name="title" value="{{$product->title}}">
              </div>
              <div class="form-group">
                <label for="body">متن محصول</label>
                <textarea class="form-control" name="body" rows="3">{{$product->body}}</textarea>
              </div>
              <div class="form-group">
                <label for="price">قیمت</label>
                <input type="text" class="form-control" name="price" id="price" value="{{$product->price}}" />
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="images" class="control-label">تصویر محصول</label>
                    <input id="images" name="images[]" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" data-msg-placeholder="چند تصویر انتخاب کنید...">
                  </div>
                </div>
                <br>
                <div class="col-sm-12">
                  <div class="row">
                    @if ($product->images)
                      @foreach ($product->images as $image)
                        <div class="d-flex flex-column">
                          <a class="p-1" href="{{ asset('storage/' . $image->url) }}" target="blanck"><img src="{{ asset('storage/' . $image->url)}}" style="width:100px;height:100px;">
                          </a>
                          <a class="btn bt-sm deleteImage" title="حذف" href="{{ route('products.deleteImage') }}" data-id="{{ $image->id }}"><i class="fas fa-trash text-danger"></i></a>
                        </div>
                      @endforeach
                    @endif
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="category_id">دسته بندی</label>
                <select name="category_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($categories as $category)
                  <option value="{{$category->id}}"
                    {{in_array(trim($category->id) , $product->categories->pluck('id')->toArray()) ? 'selected' : ''}}>
                    {{$category->name}}
                  </option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="tag_id">برچسب</label>
                <select name="tag_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($tags as $tag)
                  <option value="{{$tag->id}}"
                    {{in_array(trim($tag->id) , $product->tags->pluck('id')->toArray()) ? 'selected' : ''}}>
                    {{$tag->name}}
                  </option>
                  @endforeach
                </select>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">ویرایش محصول</button>
                <a href="{{ route('products.index') }}" class="btn btn-secondary btn-block">کنسل</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection