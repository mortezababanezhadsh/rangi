@extends('admin.layouts.master')
@section('title' , 'ثبت محصول')
@section('styles')

{{--bootstrap FileInput--}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<style>
  .file-drop-zone{
    overflow: auto;
  }
</style>

@endsection
@section('scripts')

  <!-- the main fileinput plugin file -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/fileinput.min.js"></script>
  <script>
  $(function () {
    $("#images").fileinput();
  });
  </script>

<script src="//cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
<script>
  var editor = CKEDITOR.replace('body', {
    language: 'fa',
    filebrowserUploadMethod : 'form',
    filebrowserUploadUrl: "{{route('admin.upload')}}"
  });
</script>
@endsection

@section('content')
<section>
</div>
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="head-section">
          <h4>ثبت محصول جدید</h4>
          <a href="{{ route('products.index') }}" class="btn btn-primary btn-sm">مشاهده محصولات</a>
        </div>
        <hr>
        @include('Admin.section.errors')
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="{{ route('products.store') }}" method="post"
              enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="title">عنوان</label>
                <input type="text" class="form-control" name="title" value="{{old('title')}}">
              </div>
              <div class="form-group">
                <label for="body">متن</label>
                <textarea class="form-control" name="body" id="body" rows="3">{{old('body')}}</textarea>
              </div>
              <div class="form-group">
                <label for="price">قیمت</label>
                <input type="text" class="form-control" name="price" id="price" />
              </div>
              <div class="form-group">
                <label for="images">تصاویر</label>
                <input id="images" name="images[]" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" data-msg-placeholder="چند تصویر انتخاب کنید...">
              </div>
              <div class="form-group">
                <label for="category_id">دسته بندی</label>
                <select name="category_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="tag_id">برچسب</label>
                <select name="tag_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($tags as $tag)
                  <option value="{{$tag->id}}">{{$tag->name}}</option>
                  @endforeach
                </select>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">ثبت محصول</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
