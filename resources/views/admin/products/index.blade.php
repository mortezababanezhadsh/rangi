@extends('admin.layouts.master')
@section('title' , 'محصولات')

@section('content')
<section>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="head-section">
                    <h4>محصولات</h4>
                    <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm">ثبت محصول جدید</a>
                </div>
                <p class="text-muted font-13 m-b-30">
                    تمام محصولات در زیر به نمایش گذاشته شده است.
                </p>
                <div class="table-responsive-sm">
                    <table id="datatable" class="table table-bordered table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th>عنوان</th>
                                <th>تصویر</th>
                                <th>نویسنده</th>
                                <th>قیمت</th>
                                <th>دسته ها</th>
                                <th>تنظیمات</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td><a href="{{$product->path()}}">{{$product->title}}</a></td>
                                <td>
                                    @if ($product->images)
                                    <a href="{{ asset('storage/' . $product->images[0]->url) }}" target="blanck"><img src="{{ asset('storage/' . $product->images[0]->url) }}" style="width:70px;height:70px;"></a>
                                    @endif
                                </td>
                                <td>{{$product->user->name}}</td>
                                <td>{{$product->price}}</td>
                                <td>
                                    <ul>
                                        @foreach ($product->categories->pluck('name') as $category)
                                        <li>{{$category}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    <div class="btn-group-sm d-flex justify-content-xl-between">
                                        <input type="checkbox" data-id="{{ $product->id }}" name="status"
                                            class="js-switch" {{ $product->status == 1 ? 'checked' : '' }}>
                                        <a href="{{ route('products.edit', ['product' => $product->id]) }}"
                                            class="btn btn-sm" title="ویرایش">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('products.delete', ['product' => $product->id]) }}"
                                            title="حذف" class="btn btn-sm deleted" data-id="{{ $product->id }}">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</section>
@endsection

@section('styles')
{{-- Switchery Style --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
@endsection

@section('scripts')

{{-- Switchery Scripts --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>
<script>
    let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        let switchery = new Switchery(html,  { size: 'small' });
    });
</script>

{{-- Change Status and Confirm Delete --}}
<script>
    // Change Status 
    $(document).ready(function(){
        $('.js-switch').change(function () {
        let status = $(this).prop('checked') === true ? 1 : 0;
        let id = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('products.status') }}',
            data: {'status': status, 'id': id}
            });
        });
    });

//  Delete Confirm 
    $('.deleted').on('click', function (e) {
        e.preventDefault();

        let le = $(this);
        let url = le.attr('href');
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let id = $(this).data('id');

        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'مطمئن هستید?',
            text: "بعد از حذف شما قادر به بازیابی اطلاعات نیستید!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'حذف',
            cancelButtonText: 'کنسل',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        'id': id,
                        '_token': CSRF_TOKEN
                    },
                    success: function (res) {  
                        
                        swalWithBootstrapButtons.fire(
                                'حذف شد!',
                                'اطلاعات شما به طور کامل حذف شد',
                                'success'
                            )
                            setInterval(function() {location.reload();}, 2000);
                    }
                });
            }else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                'کنسل شد',
                'اطلاعات موردنظر هنوز در دسترس شماست :)',
                'error'
                )
            }
        });
    });

</script>
@endsection