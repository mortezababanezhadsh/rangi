<script src="{{ mix('js/admin.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(document).ready(function() {
            $('#datatable').DataTable();
        } );
</script>
@yield('scripts')