<a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
  <i class="fas fa-bars"></i>
</a>

{{-- start sidebar --}}
<nav id="sidebar" class="sidebar-wrapper">
  <div class="sidebar-content">
    <div class="sidebar-brand">
      <a href="#">صفحه اصلی سایت</a>
      <div id="close-sidebar">
        <i class="fas fa-times"></i>
      </div>
    </div>
    <div class="sidebar-header">
      <div class="user-pic">
        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg"
          alt="User picture">
      </div>
      <div class="user-info">
        <span class="user-name">
          <strong>مرتضی بابانژاد</strong>
        </span>
        <span class="user-role">مدیر اصلی</span>
        <span class="user-status">
          <i class="fa fa-circle"></i>
          <span>آنلاین</span>
        </span>
      </div>
    </div>
    <!-- sidebar-header  -->
    <div class="sidebar-search">
      <div>
        <div class="input-group">
          <input type="text" class="form-control search-menu" placeholder="Search...">
          <div class="input-group-append">
            <span class="input-group-text">
              <i class="fa fa-search" aria-hidden="true"></i>
            </span>
          </div>
        </div>
      </div>
    </div>
    <!-- sidebar-search  -->
    <div class="sidebar-menu">
      <ul>
        <li class="header-menu">
          <span><small>عمومی</small></span>
        </li>
        <li>
          <a href="{{ route('admin.dashboard') }}">
            <i class="fa fa-tachometer-alt"></i>
            <span>داشبورد</span>
          </a>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>مقالات</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('articles.index') }}">مشاهده مقالات</a>
              </li>
              <li>
                <a href="{{ route('articles.create') }}">ثبت مقاله جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>محصولات</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('products.index') }}">مشاهده محصولات</a>
              </li>
              <li>
                <a href="{{ route('products.create') }}">ثبت محصول جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>منو</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('menus.index') }}">مشاهده منو</a>
              </li>
              <li>
                <a href="{{ route('menus.create') }}">ثبت آیتم جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>دسته بندی</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('categories.index') }}">مشاهده دسته ها</a>
              </li>
              <li>
                <a href="{{ route('categories.create') }}">ثبت دسته جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>برچسب</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('tags.index') }}">مشاهده برچسب ها</a>
              </li>
              <li>
                <a href="{{ route('tags.create') }}">ثبت برچسب جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>دیدگاه</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('comments.index') }}">دیدگاه های تایید شده</a>
              </li>
              <li>
                <a href="{{ route('comments.approved') }}">دیدگاه های تایید نشده</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-file"></i>
            <span>کوپن</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('coupons.index') }}">مشاهده کوپن ها</a>
              </li>
              <li>
                <a href="{{ route('coupons.create') }}">ثبت کوپن جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="header-menu">
          <span><small>خصوصی</small></span>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>حمل و نقل ها</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('transportations.index') }}">مشاهده حمل و نقل ها</a>
              </li>
              <li>
                <a href="{{ route('transportations.create') }}">ثبت حمل و نقل جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>نقش ها</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('roles.index') }}">مشاهده نقش ها</a>
              </li>
              <li>
                <a href="{{ route('roles.create') }}">ثبت نقش جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="sidebar-dropdown">
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span>مجوز ها</span>
          </a>
          <div class="sidebar-submenu">
            <ul>
              <li>
                <a href="{{ route('permissions.index') }}">مشاهده مجوز ها</a>
              </li>
              <li>
                <a href="{{ route('permissions.create') }}">ثبت مجوز جدید</a>
              </li>
            </ul>
          </div>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Documentation</span>
            <span class="badge badge-pill badge-primary">Beta</span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-calendar"></i>
            <span>Calendar</span>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-folder"></i>
            <span>Examples</span>
          </a>
        </li>
      </ul>
    </div>
    <!-- sidebar-menu  -->
  </div>
  <!-- sidebar-content  -->
  <div class="sidebar-footer">
    <a href="#">
      <i class="fa fa-bell"></i>
      <span class="badge badge-pill badge-warning notification">3</span>
    </a>
    <a href="#">
      <i class="fa fa-envelope"></i>
      <span class="badge badge-pill badge-success notification">7</span>
    </a>
    <a href="#">
      <i class="fa fa-cog"></i>
      <span class="badge-sonar"></span>
    </a>
    <a href="#">
      <i class="fa fa-power-off"></i>
    </a>
  </div>
</nav>