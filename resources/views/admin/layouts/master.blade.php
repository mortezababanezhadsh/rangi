@include('admin.layouts.header')
<body>
    <div id="app">
        <div class="page-wrapper chiller-theme toggled">
            <!-- sidebar-wrapper  -->
            @include('admin.layouts.sidebar')
            <!-- end sidebar-wrapper  -->

            <!-- page-main" -->
            <main class="page-content">
                <div class="container-fluid">

                    <!-- page-content" -->
                    @yield('content')
                    <!-- end page-content" -->

                </div>
            </main>
            <!-- end page-main" -->
        </div>
    </div>
    <!-- Scripts -->
    @include('admin.layouts.footer')
</body>
</html>