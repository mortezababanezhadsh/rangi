@extends('admin.layouts.master')
@section('title' , 'ویرایش حمل و نقل')

@section('content')
<section>
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="head-section">
          <h4>ویرایش حمل و نقل</h4>
          <a href="{{ route('transportations.index') }}" class="btn btn-primary">مشاهده حمل و نقل ها</a>
        </div>
        <hr>
        @include('Admin.section.errors')
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="{{ route('transportations.update', ['transportation' => $transportation->id]) }}"
              method="post">
              @csrf
              @method('PATCH')
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <label for="title">عنوان</label>
                    <input type="text" class="form-control" name="title" value="{{$transportation->title}}">
                </div>
                  <div class="col-md-6">
                      <label for="price">قیمت</label>
                      <input type="text" class="form-control" name="price" value="{{$transportation->price}}">
                  </div>
                </div>
              </div>
              <div class="form-group">
                  <label for="description">توضیحات</label>
                  <textarea rows="3" class="form-control" name="description">{{$transportation->description}}</textarea>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">ویرایش حمل و نقل</button>
                <a href="{{ route('transportations.index') }}" class="btn btn-secondary btn-block">کنسل</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection