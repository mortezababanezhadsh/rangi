@extends('admin.layouts.master')
@section('title' , 'ویرایش نقش ها')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
          <div class="card-box">
              <div class="head-section">
                  <h4>ویرایش نقش</h4>
                  <a href="{{ route('roles.index') }}" class="btn btn-primary btn-sm">مشاهده نقش ها</a>
              </div>
              <hr>
              @include('admin.section.errors')
              <div class="row">
                <div class="col-lg-12">
                  <form class="form-horizontal" action="{{ route('roles.update' , ['role'=> $role->id]) }}" method="post">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                      <label for="name">نام نقش</label>
                      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ $role->name }}">
                    </div>
                    <div class="form-group">
                      <label for="description">توضیحات نقش</label>
                      <input type="text" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" value="{{ $role->description }}" />
                    </div>
                    <div class="form-group">
                      <label for="permission_id">مجوزها</label>
                      <select name="permission_id[]" class="selectpicker" multiple data-selected-text-format="count > 4" data-live-search="true" data-style="btn btn-outline-primary">
                        @foreach ($permissions as $permission)
                          <option value="{{$permission->id}}" {{in_array(trim($permission->id) , $role->permissions->pluck('id')->toArray()) ? 'selected' : ''}}>{{$permission->name}} - {{$permission->description}}</option>                          
                        @endforeach  
                      </select>
                    </div>
                    <br>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-block">ویرایش نقش</button>
                      <a href="{{ route('roles.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>     
    </div>
  </section>
@endsection