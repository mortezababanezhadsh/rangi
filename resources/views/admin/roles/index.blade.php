@extends('admin.layouts.master')
@section('title' , 'نقش ها')
@section('content')
<section>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="head-section">
                    <h4>نقش ها</h4>
                    <div class="btn-group-sm">
                        <a href="{{ route('roles.create') }}" class="btn btn-primary btn-sm">ایجاد نقش جدید</a>
                        <a href="{{ route('permissions.index') }}" class="btn btn-success btn-sm">مجوزها</a>
                    </div>
                </div>
                <p class="text-muted font-13 m-b-30">
                    تمام نقش ها در زیر به نمایش گذاشته شده است.
                </p>
                <div class="table-responsive-sm">
                    <table id="datatable" class="table table-bordered table-hover dt-responsive">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>عنوان</th>
                            <th>توضیحات</th>
                            <th>اجازه دسترسی</th>
                            <th>تنظیمات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($roles as $i => $role)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$role->name}}</td>
                                <td>{{$role->description}}</td>
                                <td>
                                    @if (count($role->permissions))
                                        <ul>
                                            @foreach ($role->permissions as $permission)
                                            <li>{{$permission->name}} - {{$permission->description}}</li>
                                            @endforeach    
                                        </ul>
                                    @else
                                        <span class="text-warning">برای این نقش مجوزی داده نشد است!</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group-sm d-flex justify-content-xl-between">
                                        <a href="{{ route('roles.delete', ['role' => $role->id]) }}" title="حذف" class="btn btn-sm deleterole" data-id="{{ $role->id }}">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="{{ route('roles.edit', ['role' => $role->id]) }}" title="ویرایش" class="btn btn-sm">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</section>
@endsection

@section('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('scripts')
    {{-- Confirm Delete --}}
<script>
    //  Delete Confirm 
    $('.deleterole').on('click', function (e) {
        e.preventDefault();

        let le = $(this);
        let url = le.attr('href');
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let id = $(this).data('id');

        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'مطمئن هستید?',
            text: "بعد از حذف شما قادر به بازیابی اطلاعات نیستید!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'حذف',
            cancelButtonText: 'کنسل',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        'id': id,
                        '_token': CSRF_TOKEN
                    },
                    success: function (res) {  
                        
                        swalWithBootstrapButtons.fire(
                                'حذف شد!',
                                'اطلاعات شما به طور کامل حذف شد',
                                'success'
                            )
                            setInterval(function() {location.reload();}, 2000);
                    }
                });
            }else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                'کنسل شد',
                'اطلاعات موردنظر هنوز در دسترس شماست :)',
                'error'
                )
            }
        });
    });

</script>
@endsection