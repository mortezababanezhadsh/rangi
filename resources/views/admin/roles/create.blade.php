@extends('admin.layouts.master')
@section('title' , 'ثبت نقش')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
        <div class="card-box">
            <div class="head-section">
                <h4>ثبت نقش جدید</h4>
                <a href="{{ route('roles.index') }}" class="btn btn-primary btn-sm">مشاهده نقش ها</a>
            </div>
            <hr>
            @include('admin.section.errors')
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" action="{{ route('roles.store') }}" method="post">
                @csrf
                  <div class="form-group">
                    <label for="name">نام نقش</label> <small><span class="text-danger">به صورت انگلیسی نوشته شود.</span></small>
                    <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{old('name')}}">
                  </div>
                  <div class="form-group">
                    <label for="description">توضیحات نقش</label>
                    <input type="text" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" value="{{old('description')}}" />
                  </div>
                  <div class="form-group">
                    <label for="permission_id">مجوزها</label>
                    <select name="permission_id[]" class="selectpicker" multiple data-selected-text-format="count > 3" data-live-search="true" data-style="btn btn-outline-primary">
                      @foreach ($permissions as $permission)
                        <option value="{{$permission->id}}">{{$permission->name}} - {{$permission->description}}</option>                          
                      @endforeach  
                    </select>
                  </div>
                  <br>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">ثبت نقش</button>
                    <a href="{{ route('roles.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>     
    </div>
  </section>
@endsection