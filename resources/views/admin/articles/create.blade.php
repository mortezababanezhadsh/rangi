@extends('admin.layouts.master')
@section('title' , 'ثبت مقاله')

@section('scripts')
<script src="//cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
<script>
  var editor = CKEDITOR.replace('body', {
    language: 'fa',
    filebrowserUploadMethod : 'form',
    filebrowserUploadUrl: "{{route('admin.upload')}}"
  });
</script>
@endsection

@section('content')
<section>
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="head-section">
          <h4>ثبت مقاله جدید</h4>
          <a href="{{ route('articles.index') }}" class="btn btn-primary btn-sm">مشاهده مقالات</a>
        </div>
        <hr>
        @include('Admin.section.errors')
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="{{ route('articles.store') }}" method="post"
              enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="title">عنوان</label>
                <input type="text" class="form-control" name="title" value="{{old('title')}}">
              </div>
              <div class="form-group">
                <label for="description">توضیحات کوتاه</label>
                <textarea class="form-control" name="description" id="description" rows="3">{{old('description')}}</textarea>
              </div>
              <div class="form-group">
                <label for="body">متن</label>
                <textarea class="form-control" name="body" id="body" rows="3">{{old('body')}}</textarea>
              </div>
              <div class="form-group">
                <label for="image">تصویر</label>
                <input type="file" class="form-control" name="image" id="image" />
              </div>
              <div class="form-group">
                <label for="category_id">دسته بندی</label>
                <select name="category_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="tag_id">برچسب</label>
                <select name="tag_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($tags as $tag)
                  <option value="{{$tag->id}}">{{$tag->name}}</option>
                  @endforeach
                </select>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">ثبت مقاله</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
