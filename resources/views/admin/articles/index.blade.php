@extends('admin.layouts.master')
@section('title' , 'مقالات')

@section('content')
<section>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="head-section">
                    <h4>مقالات</h4>
                    <a href="{{ route('articles.create') }}" class="btn btn-primary btn-sm">ایجاد مقاله جدید</a>
                </div>
                <p class="text-muted font-13 m-b-30">
                    تمام مقالات در زیر به نمایش گذاشته شده است.
                </p>
                <div class="table-responsive-sm">
                    <table id="datatable" class="table table-bordered table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th>عنوان</th>
                                <th>تصویر</th>
                                <th>نویسنده</th>
                                <th>دسته ها</th>
                                <th>تنظیمات</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($articles as $article)
                            <tr>
                                <td><a href="{{$article->path()}}">{{$article->title}}</a></td>
                                <td>
                                    @if ($article->image)
                                    <a href="{{ asset('storage/' . $article->image->url) }}" target="blanck"><img src="{{ asset('storage/' . $article->image->url) }}" style="width:70px;height:70px;"></a>
                                    @endif
                                </td>
                                <td>{{$article->user->name}}</td>
                                <td>
                                    <ul>
                                        @foreach ($article->categories->pluck('name') as $category)
                                        <li>{{$category}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    <div class="btn-group-sm d-flex justify-content-xl-between">
                                        <input type="checkbox" data-id="{{ $article->id }}" name="status"
                                            class="js-switch" {{ $article->status == 1 ? 'checked' : '' }}>
                                        <a href="{{ route('articles.edit', ['article' => $article->id]) }}"
                                            class="btn btn-sm" title="ویرایش">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('articles.delete', ['article' => $article->id]) }}"
                                            title="حذف" class="btn btn-sm deleted" data-id="{{ $article->id }}">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</section>
@endsection

@section('styles')
{{-- Switchery Style --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
@endsection

@section('scripts')

{{-- Switchery Scripts --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>
<script>
    let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        let switchery = new Switchery(html,  { size: 'small' });
    });
</script>

{{-- Change Status and Confirm Delete --}}
<script>
    // Change Status 
    $(document).ready(function(){
        $('.js-switch').change(function () {
        let status = $(this).prop('checked') === true ? 1 : 0;
        let id = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('articles.status') }}',
            data: {'status': status, 'id': id}
            });
        });
    });

//  Delete Confirm 
    $('.deleted').on('click', function (e) {
        e.preventDefault();

        let le = $(this);
        let url = le.attr('href');
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let id = $(this).data('id');

        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'مطمئن هستید?',
            text: "بعد از حذف شما قادر به بازیابی اطلاعات نیستید!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'حذف',
            cancelButtonText: 'کنسل',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        'id': id,
                        '_token': CSRF_TOKEN
                    },
                    success: function (res) {  
                        
                        swalWithBootstrapButtons.fire(
                                'حذف شد!',
                                'اطلاعات شما به طور کامل حذف شد',
                                'success'
                            )
                            setInterval(function() {location.reload();}, 2000);
                    }
                });
            }else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                'کنسل شد',
                'اطلاعات موردنظر هنوز در دسترس شماست :)',
                'error'
                )
            }
        });
    });

</script>
@endsection