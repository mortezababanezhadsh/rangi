@extends('admin.layouts.master')
@section('title' , 'ویرایش مقاله')

@section('scripts')
<script src="//cdn.ckeditor.com/4.13.1/full/ckeditor.js"></script>
<script>
  var editor = CKEDITOR.replace('body', {
    language: 'fa',
    filebrowserUploadMethod : 'form',
    filebrowserUploadUrl: "{{route('admin.upload')}}"
  });
</script>
@endsection

@section('content')
<section>
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <div class="head-section">
          <h4>ویرایش مقاله</h4>
          <a href="{{ route('articles.index') }}" class="btn btn-primary">مشاهده مقالات</a>
        </div>
        <hr>
        @include('Admin.section.errors')
        <div class="row">
          <div class="col-lg-12">
            <form class="form-horizontal" action="{{ route('articles.update', ['article' => $article->id]) }}"
              method="post" enctype="multipart/form-data">
              @csrf
              @method('PATCH')
              <div class="form-group">
                <label for="title">عنوان مقاله</label>
                <input type="text" class="form-control" name="title" value="{{$article->title}}">
              </div>
              
              <div class="form-group">
                <label for="description">توضیحات کوتاه</label>
                <textarea class="form-control" name="description" id="description" rows="3">{{$article->description}}</textarea>
              </div>
              <div class="form-group">
                <label for="body">متن مقاله</label>
                <textarea class="form-control" name="body" rows="3">{{$article->body}}</textarea>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                  <label for="image" class="control-label">تصویر مقاله</label>
                  <input type="file" class="form-control" name="image" id="image"
                    placeholder="تصویر مقاله را وارد کنید">
                </div>
                <br>
                <div class="col-sm-12">
                  <div class="row">
                    @if ($article->image)
                      <a href="{{ asset('storage/' . $article->image->url) }}" target="blanck"><img src="{{ asset('storage/' . $article->image->url)}}" style="width:100px;height:100px;"></a>
                    @endif
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="category_id">دسته بندی</label>
                <select name="category_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($categories as $category)
                  <option value="{{$category->id}}"
                    {{in_array(trim($category->id) , $article->categories->pluck('id')->toArray()) ? 'selected' : ''}}>
                    {{$category->name}}
                  </option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="tag_id">برچسب</label>
                <select name="tag_id[]" class="selectpicker" multiple data-selected-text-format="count > 6"
                  data-live-search="true" data-style="btn btn-outline-primary">
                  @foreach ($tags as $tag)
                  <option value="{{$tag->id}}"
                    {{in_array(trim($tag->id) , $article->tags->pluck('id')->toArray()) ? 'selected' : ''}}>
                    {{$tag->name}}
                  </option>
                  @endforeach
                </select>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">ویرایش مقاله</button>
                <a href="{{ route('articles.index') }}" class="btn btn-secondary btn-block">کنسل</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection