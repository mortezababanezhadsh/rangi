@extends('admin.layouts.master')
@section('title' , 'ویرایش برچسب بندی')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
          <div class="card-box">
              <div class="head-section">
                  <h4>ویرایش برچسب</h4>
                  <a href="{{ route('tags.index') }}" class="btn btn-primary btn-sm">مشاهده برچسب ها</a>
              </div>
              <hr>
              @include('admin.section.errors')
              <div class="row">
                <div class="col-lg-12">
                  <form class="form-horizontal" action="{{ route('tags.update' , ['tag'=> $tag->id]) }}" method="post">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                      <label for="name">نام برچسب</label>
                      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ $tag->name }}">
                    </div>
                    <div class="form-group">
                      <label for="slug">نامک برچسب</label>
                      <input type="text" class="form-control {{ $errors->has('slug') ? 'is-invalid' : '' }}" name="slug" value="{{ $tag->slug }}" />
                    </div>
                    <br>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-block">ویرایش برچسب</button>
                      <a href="{{ route('tags.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>     
    </div>
  </section>
@endsection