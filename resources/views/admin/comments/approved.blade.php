@extends('admin.layouts.master')
@section('title' , 'دیدگاه ها')
@section('content')
<section>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="head-section">
                    <h4>دیدگاه ها</h4>
                <a href="{{ route('comments.index') }}" class="btn btn-primary btn-sm">مشاهده دیدگاههای تایید شده</a>
                </div>
                <p class="text-muted font-13 m-b-30">
                    تمام دیدگاه های تایید نشده در زیر به نمایش گذاشته شده است.
                </p>
                <div class="table-responsive-sm">
                    <table id="datatable" class="table table-bordered table-hover dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>نام کاربر</th>
                                <th>متن کامنت</th>
                                <th>پست مربوطه</th>
                                <th>تنظیمات</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($comments as $i => $comment)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$comment->user->name}}</td>
                                <td>{{$comment->comment}}</td>
                                <td><a href="{{ $comment->commentable->path() }}">{{  $comment->commentable->title }}</a></td>
                                <td>
                                    <input type="checkbox" data-id="{{ $comment->id }}" name="status" class="js-switch"
                                        {{ $comment->status == 1 ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <div class="btn-group-sm">
                                        <a href="{{ route('comments.delete', ['comment' => $comment->id]) }}"
                                            title="حذف" class="btn btn-sm deleted" data-id="{{ $comment->id }}">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</section>
@endsection

@section('styles')
{{-- Switchery Style --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
@endsection

@section('scripts')

{{-- Switchery Scripts --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>
<script>
    let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        let switchery = new Switchery(html,  { size: 'small' });
    });
</script>

{{-- Change Status and Confirm Delete --}}
<script>
    // Change Status 
    $(document).ready(function(){
        $('.js-switch').change(function () {
        let status = $(this).prop('checked') === true ? 1 : 0;
        let id = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('comments.status') }}',
            data: {'status': status, 'id': id}
            });
        });
    });

//  Delete Confirm 
    $('.deleted').on('click', function (e) {
        e.preventDefault();

        let le = $(this);
        let url = le.attr('href');
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let id = $(this).data('id');

        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'مطمئن هستید?',
            text: "بعد از حذف شما قادر به بازیابی اطلاعات نیستید!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'حذف',
            cancelButtonText: 'کنسل',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        'id': id,
                        '_token': CSRF_TOKEN
                    },
                    success: function (res) {  
                        
                        swalWithBootstrapButtons.fire(
                                'حذف شد!',
                                'اطلاعات شما به طور کامل حذف شد',
                                'success'
                            )
                            setInterval(function() {location.reload();}, 2000);
                    }
                });
            }else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                'کنسل شد',
                'اطلاعات موردنظر هنوز در دسترس شماست :)',
                'error'
                )
            }
        });
    });

</script>
@endsection