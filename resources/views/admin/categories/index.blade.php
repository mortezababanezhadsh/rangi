@extends('admin.layouts.master')
@section('title' , 'دسته بندی ها')
@section('content')
<section>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="head-section">
                    <h4>دسته بندی ها</h4>
                    <a href="{{ route('categories.create') }}" class="btn btn-primary btn-sm">ایجاد دسته جدید</a>
                </div>
                <p class="text-muted font-13 m-b-30">
                    تمام دسته بندی ها در زیر به نمایش گذاشته شده است.
                </p>
                <div class="table-responsive-sm">
                    <table id="datatable" class="table table-bordered table-hover dt-responsive">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>عنوان</th>
                                <th>نامک</th>
                                <th>وضعیت انتشار</th>
                                <th>تنظیمات</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $i => $category)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$category->name}}</td>
                                <td>{{$category->slug}}</td>
                                <td>
                                    <input type="checkbox" data-id="{{ $category->id }}" name="status" class="js-switch"
                                        {{ $category->status == 1 ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <div class="btn-group-sm d-flex justify-content-xl-between">
                                        <a href="{{ route('categories.delete', ['category' => $category->id]) }}"
                                            title="حذف" class="btn btn-sm deleted" data-id="{{ $category->id }}">
                                            <i class="fas fa-trash text-danger"></i>
                                        </a>
                                        <a href="{{ route('categories.edit', ['category' => $category->id]) }}"
                                            title="ویرایش" class="btn btn-sm">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</section>
@endsection

@section('styles')
{{-- Switchery Style --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
@endsection

@section('scripts')

{{-- Switchery Scripts --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>
<script>
    let elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        let switchery = new Switchery(html,  { size: 'small' });
    });
</script>

{{-- Change Status and Confirm Delete --}}
<script>
    // Change Status 
    $(document).ready(function(){
        $('.js-switch').change(function () {
        let status = $(this).prop('checked') === true ? 1 : 0;
        let id = $(this).data('id');
        $.ajax({
            type: "GET",
            dataType: "json",
            url: '{{ route('categories.status') }}',
            data: {'status': status, 'id': id}
            });
        });
    });

//  Delete Confirm 
    $('.deleted').on('click', function (e) {
        e.preventDefault();

        let le = $(this);
        let url = le.attr('href');
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let id = $(this).data('id');

        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'مطمئن هستید?',
            text: "بعد از حذف شما قادر به بازیابی اطلاعات نیستید!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'حذف',
            cancelButtonText: 'کنسل',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        'id': id,
                        '_token': CSRF_TOKEN
                    },
                    success: function (res) {  
                        
                        swalWithBootstrapButtons.fire(
                                'حذف شد!',
                                'اطلاعات شما به طور کامل حذف شد',
                                'success'
                            )
                            setInterval(function() {location.reload();}, 2000);
                    }
                });
            }else if (result.dismiss === Swal.DismissReason.cancel) {
                swalWithBootstrapButtons.fire(
                'کنسل شد',
                'اطلاعات موردنظر هنوز در دسترس شماست :)',
                'error'
                )
            }
        });
    });

</script>
@endsection