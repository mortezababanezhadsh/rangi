@extends('admin.layouts.master')
@section('title' , 'ویرایش دسته بندی')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
          <div class="card-box">
              <div class="head-section">
                  <h4>ویرایش دسته</h4>
                  <a href="{{ route('categories.index') }}" class="btn btn-primary btn-sm">مشاهده دسته ها</a>
              </div>
              <hr>
              @include('admin.section.errors')
              <div class="row">
                <div class="col-lg-12">
                  <form class="form-horizontal" action="{{ route('categories.update' , ['category'=> $category->id]) }}" method="post">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                      <label for="name">نام دسته</label>
                      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ $category->name }}">
                    </div>
                    <div class="form-group">
                      <label for="slug">نامک دسته</label>
                      <input type="text" class="form-control {{ $errors->has('slug') ? 'is-invalid' : '' }}" name="slug" value="{{ $category->slug }}" />
                    </div>
                    <br>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-block">ویرایش دسته</button>
                      <a href="{{ route('categories.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>     
    </div>
  </section>
@endsection