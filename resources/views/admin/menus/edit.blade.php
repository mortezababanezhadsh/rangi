@extends('admin.layouts.master')
@section('title' , 'ویرایش آیتم های منو')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
          <div class="card-box">
              <div class="head-section">
                  <h4>ویرایش آیتم</h4>
                  <a href="{{ route('menus.index') }}" class="btn btn-primary btn-sm">مشاهده آیتم ها</a>
              </div>
              <hr>
              @include('admin.section.errors')
              <div class="row">
                <div class="col-lg-12">
                  <form class="form-horizontal" action="{{ route('menus.update' , ['menu'=> $menu->id]) }}" method="post">
                    @method('PATCH')
                    @csrf
                    <div class="form-group">
                      <label for="title">عنوان آیتم</label>
                      <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" name="title" value="{{ $menu->title }}">
                    </div>
                    <div class="form-group">
                      <label for="slug">آدرس</label>
                      <input type="text" class="form-control {{ $errors->has('slug') ? 'is-invalid' : '' }}" name="slug" value="{{ $menu->slug }}" />
                    </div>
                    <div class="form-group">
                      <label for="parent_id">دسته والد</label>
                      <select name="parent_id" class="selectpicker" data-live-search="true" data-style="btn btn-outline-primary">
                        @foreach ($menuAll as $menuP)
                        <option value="0">بدون والد</option>
                        <option value="{{$menuP->id}}" {{ $menuP->id == $menu->id ? 'selected' : '' }}>{{$menuP->title}}</option>
                        @endforeach
                      </select>
                    </div>
                    <br>
                    <div class="form-group">
                      <button type="submit" class="btn btn-success btn-block">ویرایش آیتم</button>
                      <a href="{{ route('menus.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>     
    </div>
  </section>
@endsection