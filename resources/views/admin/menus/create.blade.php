@extends('admin.layouts.master')
@section('title' , 'ثبت آیتم منو')

@section('content')
  <section>
    <div class="row">
      <div class="col-12">
        <div class="card-box">
            <div class="head-section">
                <h4>ثبت آیتم جدید</h4>
                <a href="{{ route('menus.index') }}" class="btn btn-primary btn-sm">مشاهده منو</a>
            </div>
            <hr>
            @include('admin.section.errors')
            <div class="row">
              <div class="col-lg-12">
                <form class="form-horizontal" action="{{ route('menus.store') }}" method="post">
                @csrf
                  <div class="form-group">
                    <label for="title">نام آیتم</label>
                    <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" name="title" value="{{old('title')}}">
                  </div>
                  <div class="form-group">
                    <label for="slug">نامک</label>
                    <input type="text" class="form-control {{ $errors->has('slug') ? 'is-invalid' : '' }}" name="slug" value="{{old('slug')}}" />
                  </div>
                  <div class="form-group">
                    <label for="parent_id">دسته والد</label>
                    <select name="parent_id" class="selectpicker" data-live-search="true" data-style="btn btn-outline-primary">
                      @foreach ($menus as $menu)
                      <option value="0">بدون والد</option>
                      <option value="{{$menu->id}}">{{$menu->title}}</option>
                      @endforeach
                    </select>
                  </div>
                  <br>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">ثبت آیتم</button>
                    <a href="{{ route('menus.index') }}" class="btn btn-secondary btn-block">کنسل</a>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>     
    </div>
  </section>
@endsection