@extends('ui.layouts.master')
@section('scripts')
<script>
    // add to cart func
    $(function () {
        $('.add-to-cart').click(function (e) { 
            e.preventDefault();
            let el = $(this);
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            let id = el.data('id');
            let url = el.attr('href');

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    'id': id,
                    '_token': CSRF_TOKEN
                },
            });
        });
    });
</script>
@endsection

@section('content')
<div class="col-md-4">
    <div class="product_sidebar">
        <div class="single_sedebar">
            <form action="#">
                <input type="text" name="#" placeholder="جستجو...">
                <i class="ti-search"></i>
            </form>
        </div>
        <div class="single_sedebar">
            <div class="select_option">
                <div class="select_option_list">دسته بندی<i class="right fa-caret-down fas"></i> </div>
                <div class="select_option_dropdown" style="display: none;">
                    {{-- @foreach ($categories as $category)
                        <p><a href="#">{{$category->name}}</a></p>
                    @endforeach --}}
                    <p><a href="#">Category 1</a></p>
                </div>
            </div>
        </div>
    <div class="single_sedebar">
        <div class="select_option">
            <div class="select_option_list">قیمت<i class="right fa-caret-down fas"></i> </div>
                <div class="select_option_dropdown" style="display: none;">
                    <p><a href="#">Type 1</a></p>
                    <p><a href="#">Type 2</a></p>
                    <p><a href="#">Type 3</a></p>
                    <p><a href="#">Type 4</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="product_list">
        <div class="row">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    <p>{{session()->get('message')}}</p>
                </div>
            @endif
            @foreach ($products as $product)
            <div class="col-lg-6 col-sm-6">
                <div class="single_product_item">
                <img src="{{ asset('storage/' . $product->images[0]->url) }}" alt="#" class="img-fluid">
                <h3 id="title"> <a href="{{ $product->path() }}">{{$product->title}}</a> </h3>
                <p id="price">{{$product->price}}  تومان</p>
                <a href="{{ route('cart.store') }}" class="btn_3 btn-sm add-to-cart" data-id="{{$product->id}}">افزودن به سبد خرید
                    <i class="flaticon-shopping-cart-black-shape"></i>
                </a>
                </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
{!! $products->links() !!}


{{-- <nav class="blog-pagination justify-content-center d-flex">
    <ul class="pagination">
        <li class="page-item">
            <a href="#" class="page-link" aria-label="Previous">
                <i class="ti-angle-right"></i>
            </a>
        </li>
        <li class="page-item">
            <a href="#" class="page-link">1</a>
        </li>
        <li class="page-item active">
            <a href="#" class="page-link">2</a>
        </li>
        <li class="page-item">
            <a href="#" class="page-link" aria-label="Next">
                <i class="ti-angle-left"></i>
            </a>
        </li>
    </ul>
</nav> --}}
@endsection