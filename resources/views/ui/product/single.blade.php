@extends('ui.layouts.master')
@section('styles')

@endsection
@section('content') 

<div class="product_image_area padding_top section_padding">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-lg-12">
            <div class="row gallery-item"  style="margin-top:150px;">
               @foreach ($product->images as $key => $image)
               <div class="col-md-4">
                  <a href="{{ asset('storage/' . $image->url) }}" class="img-pop-up img-gal">
                     <div class="single-gallery-image" style="background: url({{ asset('storage/' . $image->url) }});">
                     </div>
                  </a>
               </div>
               @endforeach
            </div>
         </div>
         <div class="col-lg-12">
            <div class="single_product_text text-center">
               <h3>{{ $product->title }}</h3>
               <p>
               {!! $product->body !!}
               </p>
         <div class="card_area">
    <div class="product_count_area">
   <p>تعداد</p>
   <div class="product_count d-inline-block">
            <span class="product_count_item number-increment"> <i class="ti-minus"></i></span>
            <input class="product_count_item input-number" type="text" value="1" min="0" max="10">
                  <span class="product_count_item inumber-decrement"> <i class="ti-plus"></i></span>
                  </div>
                  <p>{{ $product->price }} تومان</p>
                  </div>
                     <div class="add_to_cart">
                        <a href="" class="btn_3">افزودن به سبد خرید
                           <i class="flaticon-shopping-cart-black-shape"></i>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
   
   
   <section class="subscribe_part section_padding">
   <div class="container">
   <div class="row justify-content-center">
   <div class="col-lg-8">
   <div class="subscribe_part_content">
   <h2>اطلاع از تخفیفات!</h2>
   <p>لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه طراحی یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها چگونه در نظر گرفته شده‌است. از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند.</p>
   <div class="subscribe_form">
   <input type="email" placeholder="ایمیل خود را وارد کنید.">
   <a href="#" class="btn_1">عضویت</a>
   </div>
   </div>
   </div>
   </div>
   </div>
   </section>

   <div class="container">
      <div class="row justify-content-center">
      <div class="col-lg-8">
@include('ui.partials.comment', ['comments' => $comments , 'subject' => $product])
</div>
</div>
</div>
@endsection

@section('scripts')
   <script>
      $("#sendCommentModal").on("show.bs.modal", function(event) {
      let button = $(event.relatedTarget);
      let replyId = button.data("reply");
      let modal = $(this);
      modal.find("[name='reply_id']").val(replyId);
   });
   </script>
   <script src="node_modules/@glidejs/glide/dist/glide.min.js"></script>

   <script>
      $('.slider').glide({
         autoplay: true,
         arrowsWrapperClass: 'slider-arrows',
         arrowRightText: '',
         arrowLeftText: ''
      });
   </script>
@endsection