@foreach ($childComment as $comment)
   <div class="comment-list pl-3" >
      <div class="single-comment justify-content-between d-flex">
         <div class="user justify-content-between d-flex">
            <div class="thumb d-flex flex-column">
               <img src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="">
               <h5 class="mt-1">
                  <a href="#">{{ $comment->user->name }}</a>
                  </h5>
            </div>
            <div class="desc">
               <p class="comment">
                  {{ $comment->comment }}
               </p>
               <div class="d-flex justify-content-between">
                  <div class="d-flex align-items-center">
                     <p class="date">{{ jdate($comment->created_at)->format('%d %B، %Y') }} | {{ jdate($comment->created_at)->ago() }}</p>
                     <a href="#" class="btn-reply text-uppercase" data-toggle="modal" data-target="#sendCommentModal" data-reply="{{ $comment->id }}">پاسخ</a>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @if (count($comment->childs))
      @include('ui.partials.replyComment',['childComment' => $comment->childs])
   @endif
      <br>  
@endforeach
<br><br>