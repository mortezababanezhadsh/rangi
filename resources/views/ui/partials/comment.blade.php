<div class="comments-area">
  <h4>05 دیدگاه</h4>
  @foreach ($comments as $comment)
  <div class="comment-list">
    <div class="single-comment justify-content-between d-flex">
       <div class="user justify-content-between d-flex">
          <div class="thumb d-flex flex-column">
             <img src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="">
             <h5 class="mt-1">
               <a href="#">{{ $comment->user->name }}</a>
             </h5>
            </div>
          <div class="desc">
             <p class="comment">
                {{ $comment->comment }}
             </p>
             <br>
             <div class="d-flex justify-content-between">
                <div class="d-flex align-items-center">
                   <p class="date">{{ jdate($comment->created_at)->format('%d %B، %Y') }} | {{ jdate($comment->created_at)->ago() }}</p>
                </div>
                <div class="reply-btn">
                   <a href="#" class="btn-reply text-uppercase" data-toggle="modal" data-target="#sendCommentModal" data-reply="{{ $comment->id }}">پاسخ</a>
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
   @if (count($comment->childs))
      @include('ui.partials.replyComment',['childComment' => $comment->childs])
   @endif
   @endforeach
</div>

{!! $comments->render() !!}

@if (auth()->check())
<div class="comment-form">
  <h4>ارسال دیدگاه</h4>
  @include('admin.section.errors')
  <form class="form-contact comment_form" action="/comment" id="commentForm" method="POST">
    @csrf
      <input type="hidden" name="parent_id" value="0">
      <input type="hidden" name="commentable_id" value="{{ $subject->id }}">
      <input type="hidden" name="commentable_type" value="{{ get_class($subject) }}">
     <div class="row">
        <div class="col-12">
           <div class="form-group">
              <textarea class="form-control w-100" name="comment" id="comment" cols="30" rows="9"
                 placeholder="دیدگاه شما..."></textarea>
           </div>
        </div>
     </div>
     <div class="form-group mt-3">
        <button type="submit" href="#" class="btn_3 button-contactForm">ثبت دیدگاه</button>
     </div>
  </form>
</div>
<div id="commentMsg"></div>

@else
    <div class="alert alert-danger">
      برای ارسال دیدگاه باید ابتدا <a href="http://localhost:8000/login">وارد سایت</a> شوید.
    </div>
@endif
    <!-- Modal Reply-->
 
<div class="modal fade" id="sendCommentModal" tabindex="-1" role="dialog" aria-labelledby="sendCommentModalLabel">
   <div class="modal-dialog" role="document">
       <div class="modal-content">
           <div class="modal-body">
               <div class="row">
                  <div class="col-md-10 mx-auto">
                        <div class="comment-form">
                              <h4>ارسال پاسخ دیدگاه</h4>
                              @include('admin.section.errors')
                              <form class="form-contact comment_form" action="/comment" id="commentForm" method="POST">
                                @csrf
                                  <input type="hidden" name="reply_id" value="0">
                                  <input type="hidden" name="commentable_id" value="{{ $subject->id }}">
                                  <input type="hidden" name="commentable_type" value="{{ get_class($subject) }}">
                                 <div class="row">
                                    <div class="col-12">
                                       <div class="form-group">
                                          <textarea class="form-control w-100" name="comment" id="comment" cols="10" rows="3"
                                             placeholder="پاسخ شما..."></textarea>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="form-group mt-3">
                                    <button type="submit" href="#" class="btn_3 button-contactForm btn-sm">ثبت پاسخ</button>
                                 </div>
                              </form>
                           </div>
                  </div>
               </div>
         </div>
         <div class="modal-footer">
               <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">بستن</button>
             </div>
       </div>
   </div>
</div>

{{-- @section('scripts')
    <script>
       $(document).ready(function () {
          $('#commentForm').on('submit', function (e) {
            e.preventDefault();
            let form_data = $(this).serialize();
            $.ajax({
               url: "route('/comment')",
               method: "POST",
               data: form_data,
               dataType: "JSON",
               success: function (data){
                  if (data.error != '') {
                     $('#commentForm')[0].reset();
                     $('#commentMsg').html(data.error);
                  }
               }
            })
          });
       });
    </script>
@endsection --}}