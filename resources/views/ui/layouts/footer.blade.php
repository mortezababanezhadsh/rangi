
    <!--::footer_part start::-->
    <footer class="footer_part">
      <div class="footer_iner section_bg">
          <div class="container">
              <div class="row justify-content-between align-items-center">
                  <div class="col-lg-8">
                      <div class="footer_menu">
                          <div class="footer_logo">
                              <a href="index.html"><img src="{{ asset('ui/img/rangi.png') }}" alt="#"></a>
                          </div>
                          <div class="footer_menu_item">
                            @foreach ($menus as $menu)
                                <a href="{{$menu->slug}}">{{$menu->title}}</a>
                            @endforeach
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-4">
                      <div class="social_icon">
                          <a href="#"><i class="fab fa-facebook-f"></i></a>
                          <a href="#"><i class="fab fa-instagram"></i></a>
                          <a href="#"><i class="fab fa-google-plus-g"></i></a>
                          <a href="#"><i class="fab fa-linkedin-in"></i></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      
      <div class="copyright_part">
          <div class="container">
              <div class="row ">
                  <div class="col-lg-12">
                      <div class="copyright_text">
                          <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
                          <div class="copyright_link">
                              <a href="#">Turms & Conditions</a>
                              <a href="#">FAQ</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
</footer>
<!--::footer_part end::-->
{{-- <script src="{{ asset('/ui/js/ui') }}"></script> --}}
<script
  src="https://code.jquery.com/jquery-1.12.4.js"
  integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
  crossorigin="anonymous"></script>

    <!-- popper js -->
    <script src="/ui/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="/ui/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="/ui/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="/ui/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="/ui/js/mixitup.min.js"></script>
    <!-- particles js -->
    <script src="/ui/js/owl.carousel.min.js"></script>
    {{-- <script src="/ui/js/jquery.nice-select.min.js"></script> --}}
    <!-- slick js -->
    <script src="/ui/js/slick.min.js"></script>
    <script src="/ui/js/jquery.counterup.min.js"></script>
    <script src="/ui/js/waypoints.min.js"></script>
    <script src="/ui/js/contact.js"></script>
    <script src="/ui/js/jquery.ajaxchimp.min.js"></script>
    <script src="/ui/js/jquery.form.js"></script>
    <script src="/ui/js/jquery.validate.min.js"></script>
    <script src="/ui/js/mail-script.js"></script>
    <!-- custom js -->
    <script src="/ui/js/custom.js"></script>
    @yield('scripts')
</body>

</html>