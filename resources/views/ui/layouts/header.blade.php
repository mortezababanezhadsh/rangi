<!doctype html>
<html lang="zxx">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>رنگی رنگی</title>
    <link rel="icon" href="{{ asset('ui/img/favicon.png') }}">
    <!-- Bootstrap CSS -->
    <!-- animate CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">

    <!-- Bootstrap CSS -->
    {{-- <link rel="stylesheet" href="/ui/css/bootstrap.min.css"> --}}
    <!-- animate CSS -->
    <link rel="stylesheet" href="/ui/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="/ui/css/owl.carousel.min.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="/ui/css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="/ui/css/flaticon.css">
    <link rel="stylesheet" href="/ui/css/themify-icons.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="/ui/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="/ui/css/slick.css">
    <!-- style CSS -->
    {{-- <link rel="stylesheet" href="/ui/css/nice-select.css"> --}}
    <link rel="stylesheet" href="/ui/css/style.css">
    <link rel="stylesheet" href="/ui/css/bootstrap.min.rtl.css">
    <link rel="stylesheet" href="/ui/css/fonts.css">
    @yield('styles')
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.html"> <img src="{{ asset('ui/img/rangi.png') }}" alt="logo" width="100px;"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu_icon"><i class="fas fa-bars"></i></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                @if (count($menus))
                                    @foreach ($menus as $menu)
                                    <li class="nav-item {{ count($menu->childs) ? 'dropdown' :'' }}">

                                        @if (count($menu->childs))
                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_{{$menu->id}}"
                                                role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{$menu->title}}
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown_{{$menu->id}}">
                                                @foreach ($menu->childs as $child)
                                                    <a class="dropdown-item" href="{{$child->slug}}">{{$child->title}}</a>
                                                @endforeach
                                            </div>
                                        @else
                                            <a class="nav-link" href="{{$menu->slug}}"> {{$menu->title}} </a>
                                        @endif                                            
                                    </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="hearer_icon d-flex align-items-center">
                            <a id="search_1" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <a href="{{ route('cart.index') }}">
                                <i class="flaticon-shopping-cart-black-shape"></i>
                            </a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="search_input" id="search_input_box">
            <div class="container ">
                <form class="d-flex justify-content-between search-inner">
                    <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                    <button type="submit" class="btn"></button>
                    <span class="ti-close" id="close_search" title="Close Search"></span>
                </form>
            </div>
        </div>
    </header>
    <!-- Header part end-->
