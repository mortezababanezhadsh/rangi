@include('ui.layouts.header')
    {{-- <!-- breadcrumb part start-->
    <section class="breadcrumb_part">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <h2>blog</h2>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    <!-- breadcrumb part end-->

    <!--================Blog Area =================-->
    <section class="blog_area section_padding">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
@include('ui.layouts.footer')