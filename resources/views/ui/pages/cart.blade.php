@extends('ui.layouts.master')

@section('scripts')
<script>
    $(function () {
        function getTotal() {
            let total = 0;

            $(".row-total-price").each(function () {
                let sum = Number($(this).html());
                total += sum;
            });
            $(".total").html(total);

        }
        getTotal();
        
        $("#btnCoupon").prop("disabled", "disabled");

        $("#discount").on("keyup", function () {
        if ($.trim($(this).val()) != "") {
            $("#btnCoupon").prop("disabled", false);
        } else {
            $("#btnCoupon").prop("disabled", "disabled");
        }
        });

        $('#apply_discount').submit(function (e) { 
            e.preventDefault();


            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            let code = $("#discount").val();
           
            $.ajax({
                type: "POST",
                url: "{{ route('checkout.applyDiscount') }}",
                data: {'_token': CSRF_TOKEN, 'code': code},
                dataType: "json",
                success: function (response) {
                    let price = $('#totalPayment').html();
                    let discount_amount = price * (response.discount_amount);
                    let totalPayment = price - discount_amount;

                    $('#discount').prop("readonly", true);
                    $("#btnCoupon").prop("disabled", "disabled");
                    $('#discount').val('');

                    $('#discount_amount').html(discount_amount);
                    $('#totalPayment').html(totalPayment);
                }
                
            });
        });

        $(".quantity").change(function() { 
            let el = $(this);
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            let id = el.data('id');

            let tr = $(this).closest('tr');
            let price = tr.find('.price').html();
            let quantity = tr.find('.quantity').val();

            let sum = price * quantity;
            tr.find('.row-total-price').html(sum);
            getTotal();

            $.ajax({
                type: "PATCH",
                url: "/cart/" + id,
                data: {'_token': CSRF_TOKEN, 'id': id, 'quantity': quantity},
                dataType: "json",
                success: function (response) {
                    
                }
            });


            
        }); 

        $('.remove-from-cart').click(function (e) {
            e.preventDefault();

            let el = $(this);
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            let id = el.data('id');
            let url = el.attr('href');

            $.ajax({
                type: "DELETE",
                url: url,
                data: {"id" : id , '_token' : CSRF_TOKEN},
                dataType: "json",
                success: function (response) { 
                    // Remove row from HTML Table
                    $(el).closest('tr').fadeOut(300,function(){
                        $(el).closest('tr').remove();
                        window.location.reload();
                    });
                 }
            });
        });
    });
</script>
@endsection

@section('content')
<div class="col-md-12">
    <section class="cart_area section_padding">
        <div class="container">
            <div class="cart_inner">
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                @if (json_decode(request()->cookie('products'), true))
                <div class="table-responsive">
                    <table class="table" id="cart_product_table">
                        <thead>
                            <tr>
                                <th scope="col">محصول</th>
                                <th scope="col">قیمت</th>
                                <th scope="col">تعداد</th>
                                <th scope="col">مبلغ</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (json_decode(request()->cookie('products'), true) as $id => $product)
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="{{ asset('storage/' . $product['image']) }}" alt="">
                                        </div>
                                        <div class="media-body p-3">
                                            <p><a href="products/{{$product['slug']}}">{{$product['title']}}</a></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5><span class="price">{{$product['price']}}</span> تومان</h5>
                                </td>
                                <td>
                                    <input class="form-control quantity" type="number" data-id="{{$id}}" value="{{$product['quantity']}}" min="1" max="10" style="width: 60px;" />
                                </td>
                                <td>
                                    <h5><span class="row-total-price">{{$product['price'] * $product['quantity']}}</span> تومان</h5>
                                </td>
                                <td>
                                    <a href="{{ route('cart.destroy', ['cart'=>$id]) }}" data-id="{{$id}}"  class="text-danger remove-from-cart">
                                        <i class="fas fa-trash text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-between">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header text-success">
                                <div class="d-flex justify-content-between">
                                    <span class="col-md-6">مبلغ قابل پرداخت:     </span>
                                    <span class="col-md-6 text-left"><span class="total" id="totalPayment"></span>  تومان</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br><br>

                <div class="checkout_btn_inner d-flex justify-content-between">
                    <a class="btn_1" href="/checkout">ادامه ثبت سفارش</a>
                    <a class="btn_1 checkout_btn_1" href="/blog">بازگشت به صفحه محصولات</a>
                </div>              
                @else
                    <div class="alert alert-success text-center">
                        <p>سبد خرید شما خالی است.</p>
                        <a class="btn text-warning" href="/products">بازگشت به صفحه محصولات</a>
                    </div>
                @endif

            </div>
        </div>
    </section>
</div>
@endsection
