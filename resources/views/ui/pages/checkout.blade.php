@extends('ui.layouts.master')

@section('scripts')
<script>
    // function getTotal() {
    //         let total = 0;

    //         $(".row-total-price").each(function () {
    //             let sum = Number($(this).html());
    //             total += sum;
    //         });
    //         $("#total").html(total);

    //     }
    //     getTotal();


    $("input[name='transportation']").change(function() {
        let id = $(this).attr('value');
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            type: "POST",
            url: "{{ route('checkout.applyDiscount') }}",
            data: {'_token': CSRF_TOKEN, 'id': id},
            dataType: "json",
            success: function (response) {
                if (response.transportation_price) {
                    let transportation_price = `
                        <span>هزینه حمل و نقل</span>
                        <span><span id="transportation_price">${response.transportation_price}</span> تومان</span>
                    `;
                    
                $('#transportation_cost').html(transportation_price)
                }
            }
        });
    });


    $('#apply_discount').submit(function (e) { 
        e.preventDefault();

        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let code = $("#discount").val();
        
        $.ajax({
            type: "POST",
            url: "{{ route('checkout.applyDiscount') }}",
            data: {'_token': CSRF_TOKEN, 'code': code},
            dataType: "json",
            success: function (response) {

                if (response.discount_price) {
                    let discount_price = `
                        <span>تخفیف</span>
                        <span><span id="discount_price">${response.discount_price}</span> تومان</span>
                    `;

                $('#coupon_discount').html(discount_price)
                }
                if (response.totalPayment) {
                    $('#totalPayment').html(response.totalPayment);
                }
            }
        });
    });

    
    $('#province_id').change(function(){
        let province_id = $(this).val();
        if(province_id){
            let url = "{{route('checkout.city')}}";
            let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type:"POST",
                url:url, 
                data: {
                    '_token': CSRF_TOKEN, 'province_id': province_id
                },
                dataType: "json",
                success:function(res)
                {     
                    if(res)
                    {
                        $("#city_id").empty();
                        $("#city_id").append('<option value="">شهر موردنظر خود را انتخاب کنید</option>');
                        
                        $.each(res,function(key,value){
                            let option = `<option value="${value.id}">${value.name}</option>`;
                            $("#city_id").append(option);
                        });
                    }
                }
            });
        }
    });




    function getTotalPayment(){
            let discount_price = $('#discount_price').html();
            let transportation_price = $('#transportation_price').html();
            let products_price = $('#total').html();

            console.log(discount_price, transportation_price, products_price);
        }
        getTotalPayment();


</script>
@endsection

@section('content')
<section class="checkout_area section_padding">
    <div class="container"> 
        <div class="billing_details">
            <div class="row">
                <div class="col-md-12">
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="coupon">
                        <div class="check_title">
                            <a  data-toggle="collapse" href="#apply_discount">
                        کد تخفیف دارید?
                            </a>
                        </div>
                        <form action="{{ route('checkout.applyDiscount') }}" method="post" class="collapse" id="apply_discount">
                            @csrf
                            <div class="col-md-6 input-group form-group">
                                <input class="form-control" id="discount" placeholder="کد تخفیف خود را وارد کنید" type="text">
                                <button type="submit" class="input-group-addon btn btn-success" id="btnCoupon" >اعمال</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-7 pt-3">
                    <h3>جزئیات صورتحساب</h3>
                    <form class="row contact_form" action="#" method="post" novalidate="novalidate">
                        <div class="col-md-6 form-group p_star">
                            <input type="text" class="form-control" id="name" name="name">
                            <span class="placeholder" data-placeholder="نام"></span>
                        </div>
                        <div class="col-md-6 form-group p_star">
                            <input type="text" class="form-control" id="family" name="family">
                            <span class="placeholder" data-placeholder="نام خانوادگی"></span>
                        </div>
                        <div class="col-md-6 form-group p_star">
                            <input type="text" class="form-control" id="phone" name="phone">
                            <span class="placeholder" data-placeholder="شماره همراه"></span>
                        </div>
                        <div class="col-md-6 form-group p_star">
                            <input type="text" readonly class="form-control" value="{{auth()->user()->email}}" id="email" name="email">
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <select id="province_id" name="province_id" class="form-control">
                                <option value="">استان مورد نظر خود را انتخاب کنید.</option>
                                @foreach ($provinces as $province)
                                    <option value="{{$province->id}}">{{$province->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <select id="city_id" name="city_id" class="form-control"></select>
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <input type="text" class="form-control" id="address" name="address">
                            <span class="placeholder" data-placeholder="آدرس"></span>
                        </div>
                        <div class="col-md-12 form-group p_star">
                            <input type="text" class="form-control" id="postal_code" name="postal_code">
                            <span class="placeholder" data-placeholder="کد پستی"></span>
                        </div>
                        <div class="col-md-12 form-group">
                            <textarea class="form-control" name="description" id="description" rows="1" placeholder="توضیحات سفارش"></textarea>
                        </div>
                    </form>
                </div>
                <div class="col-lg-5">
                    <div class="order_box">
                        <h2>سفارش شما</h2>
                            <div class="d-flex justify-content-between">
                                <span>محصول</span>
                                <span>تعداد</span>
                                <span>قیمت</span>
                            </div>
                            <br>
                            @foreach ($products as $id => $product)
                                <div class="d-flex justify-content-between">
                                    <a href="/products/{{$product['slug']}}"><small>{{$product['title']}}</small></a>
                                    <span class=""><small>{{$product['quantity']}} عدد</small></span>

                                    <span class=""><small><span class="row-total-price">{{$product['price'] * $product['quantity']}}</span> تومان</small></span>
                                </div>
                            @endforeach
                            <br>
                            <div class="d-flex justify-content-between text-primary pb-3">
                                <span>مبلغ کل سفارش</span>
                                <div>
                                    <span id="total">{{$total}}</span><span> تومان</span>
                                </div>
                            </div>
                        <div id="transportation">
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label text-nowrap col-sm-2" style="font-size: 11px;">حمل و نقل</legend>
                                    <div class="col-sm-10">
                                    @foreach ($transportations as $transportation)
                                        <div class="form-check">
                                            <input class="form-check-input" {{ $loop->first ? 'checked' : ''}} type="radio" name="transportation" id="transportation_{{$transportation->id}}" value="{{$transportation->id}}">
                                            <label class="form-check-label" for="gridRadios2" style="font-size: 11px;">
                                                {{$transportation->title}} - {{$transportation->description}} : <span class="text-danger">{{$transportation->price}} تومان</span>
                                            </label>
                                        </div>
                                        <br>
                                    @endforeach
                                    </div>
                                </div>
                                </fieldset>
                        </div>
                        <div id="payment_box">
                            <div id="transportation_cost" class="d-flex justify-content-between text-danger pb-3">
                                <span>هزینه حمل و نقل</span>
                                <span><span id="transportation_price">{{$transportation_price}}</span> تومان</span>
                            </div> 
                            <div id="coupon_discount" class="d-flex justify-content-between text-danger pb-3"></div> 
                            <div id="total_payment" class="d-flex justify-content-between text-success pb-3">
                                <span>مبلغ قابل پرداخت</span>
                                <span><span id="totalPayment">{{$totalPayment}}</span> تومان</span>
                            </div>
                        </div>
                        <div class="text-center">
                            <input id="payment_method_bankmellat" type="radio" class="input-radio" name="payment_method" value="bankmellat" checked="checked" >
                        
                            <label for="payment_method_bankmellat">
                                بانک ملت <img src="https://shop.rangirangi.com/wp-content/plugins/mellat-woocommerce/assets/images/logo.png" alt="بانک ملت">	</label>
                                    <div class="payment_box payment_method_bankmellat">
                                    <p style="font-size: 11px;">پرداخت امن به وسیله کلیه کارت های عضو شتاب از طریق درگاه بانک ملت</p>
                                </div>
                            </div>
                        <a class="btn_3" href="#">ثبت سفارش و پرداخت</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
