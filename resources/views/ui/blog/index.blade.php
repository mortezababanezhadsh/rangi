@extends('ui.layouts.master')
@section('content')
@if ($articles->count() > 0)
<div class="col-lg-8 mb-5 mb-lg-0">
    <div class="blog_left_sidebar">
@foreach ($articles as $article)
<article class="blog_item">
    <div class="blog_item_img">
        <img class="card-img rounded-0" src="{{ asset('storage/' . $article->image->url) }}" alt=""  width="700px;" height="350px;">
        <a href="#" class="blog_item_date">
            <p><span>{{ jdate($article->created_at)->format('%d %B، %Y') }}</span></p>
        </a>
    </div>
    <div class="blog_details">
        <a class="d-inline-block" href="{{ $article->path() }}">
            <h2>{{$article->title}}</h2>
        </a>
        <p>{!! $article->description !!}</p>
        <ul class="blog-info-link">
            <li><a href="#"><i class="far fa-user"></i>
                @foreach ($article->categories as $category)
                <a href="{{ route('categories', ['categorySlug'=>$category->slug]) }}">
                        {{ $category->name }}
                        
                  </a>
                  @if (!$loop->last)
                  ,
                    @endif
                @endforeach
            </a></li>
            <li><a href="#"><i class="far fa-comments"></i> 03 دیدگاه</a></li>
        </ul>
    </div>
</article>
@endforeach
</div>
</div>

@endif
<div class="col-lg-4">
    <div class="blog_right_sidebar">
        @include('ui.layouts.sidebar')
    </div>
 </div>
{!! $articles->links() !!}


{{-- <nav class="blog-pagination justify-content-center d-flex">
    <ul class="pagination">
        <li class="page-item">
            <a href="#" class="page-link" aria-label="Previous">
                <i class="ti-angle-right"></i>
            </a>
        </li>
        <li class="page-item">
            <a href="#" class="page-link">1</a>
        </li>
        <li class="page-item active">
            <a href="#" class="page-link">2</a>
        </li>
        <li class="page-item">
            <a href="#" class="page-link" aria-label="Next">
                <i class="ti-angle-left"></i>
            </a>
        </li>
    </ul>
</nav> --}}
@endsection