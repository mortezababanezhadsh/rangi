<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            // get coupons column name in website: https://laracasts.com/discuss/channels/laravel/discount-codes-table-design

            $table->string('code');
            $table->string('description');

            $table->unsignedInteger('discount_amount');
            // number of used coupon
            $table->unsignedInteger('uses')->nullable();

            $table->timestamp('expires_at');         
            $table->boolean('status')->default(1);

            $table->timestamps();
        });

        Schema::create('coupon_user', function (Blueprint $table) {
            $table->unsignedBigInteger('coupon_id');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->boolean('status')->default(0);

            $table->primary(['coupon_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_product');
        Schema::dropIfExists('coupon_user');
        Schema::dropIfExists('coupons');
    }
}
